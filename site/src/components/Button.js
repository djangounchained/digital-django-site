/* eslint-disable react/jsx-props-no-spreading */
import React from "react"
import * as PropTypes from "prop-types"
import classNames from "classnames"
import ScaleLoader from "react-spinners/ScaleLoader"

const classes = {
    raised: "block bg-primary px-8 py-2 shadow text-light simpleTransition",
    text: "block text-primary px-2 py-2 hover:bg-black10 simpleTransition",
}

const Button = ({
    component,
    children,
    variant,
    className,
    disabled,
    showLoader,
    ...buttonProps
}) => {
    const Component = component
    return (
        <Component
            className={classNames([
                classes[variant],
                className,
                { "bg-primary": variant === "raised" && !disabled },
                { "bg-primary70": variant === "raised" && disabled },
                { "text-primary": variant === "text" && !disabled },
                { "text-primary70": variant === "text" && disabled },
            ])}
            disabled={disabled}
            {...buttonProps}
        >
            { (showLoader ? (
                <ScaleLoader height={24} color="#444054" />
            ) : children)}
        </Component>
    )
}

Button.propTypes = {
    children: PropTypes.node,
    component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    variant: PropTypes.oneOf(["raised", "text"]),
    className: PropTypes.string,
    showLoader: PropTypes.bool,
    disabled: PropTypes.bool,
}

Button.defaultProps = {
    children: "",
    className: "",
    component: "button",
    variant: "raised",
    showLoader: false,
    disabled: false,
}

export default Button
