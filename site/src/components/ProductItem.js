import React from "react"
import * as PropTypes from "prop-types"
import Typography from "./Typography"

const ProductItem = ({
    title,
    description,
    icon,
}) => {
    const Icon = icon
    return (
        <div className="mb-16">
            <Icon className="w-24 h-24 mx-auto mb-8 text-accent" />
            <Typography align="center" variant="h3" gutterBottom>{title}</Typography>
            <Typography align="justify">{description}</Typography>
        </div>
    )
}

ProductItem.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.func]).isRequired,
}

export default ProductItem
