import React from "react"
import * as PropTypes from "prop-types"
import { Link } from "gatsby"
import Img from "gatsby-image"
import Typography from "./Typography"

const PortfolioItem = ({ item }) => (
    <Link cover to={item.fields.slug} bg="#444054" className="relative block square-item">
        <div className="absolute inset-0">
            {item.frontmatter.featuredImage && (
                <Img fluid={item.frontmatter.featuredImage.childImageSharp.fluid} />
            )}
        </div>

        <div
            className="
                    bg-primary70 absolute inset-0
                    flex flex-col justify-center items-center
                    opacity-0 hover:opacity-100 simpleTransition
                "
        >
            <Typography variant="h3" color="light" align="center">{item.frontmatter.title}</Typography>
        </div>
    </Link>
)

PortfolioItem.propTypes = {
    item: PropTypes.shape({
        fields: PropTypes.shape({
            slug: PropTypes.string.isRequired,
        }).isRequired,
        frontmatter: PropTypes.shape({
            title: PropTypes.string.isRequired,
            featuredImage: PropTypes.object,
            tech: PropTypes.arrayOf(PropTypes.string),
            website: PropTypes.string,
            status: PropTypes.string,
        }).isRequired,
        html: PropTypes.string,
    }).isRequired,
}

PortfolioItem.defaultProps = {
}

export default PortfolioItem
