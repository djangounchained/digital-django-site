import React from "react"
import * as PropTypes from "prop-types"
import { Link, navigate } from "gatsby"
import Typography from "./Typography"
import Button from "./Button"
import Footer from "./Footer"

const Layout = ({ children, title }) => (
    <>
        <div className="fixed top-0 w-full z-50">
            <div className="container mx-auto p-4 bg-black2 flex items-center justify-between">
                <Typography variant="h3">{title}</Typography>
                <Button
                    component={Link}
                    cover
                    direction="right"
                    bg="#444054"
                    to="/"
                    variant="text"
                    onClick={(e) => {
                        e.preventDefault()
                        navigate(-1)
                    }}
                >
                    Back home
                </Button>
            </div>
        </div>

        <div className="container mx-auto px-4 mt-16 mb-40">
            {children}
        </div>

        <Footer />
    </>
)

Layout.propTypes = {
    children: PropTypes.node,
    title: PropTypes.string,
}

Layout.defaultProps = {
    children: "",
    title: undefined,
}

export default Layout
