import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import { Fade } from "react-awesome-reveal"
import SectionContainer from "./SectionContainer"
import PortfolioItem from "./PortfolioItem"

const PortfolioSection = () => {
    const data = useStaticQuery(graphql`
    query {
        items: allMarkdownRemark(
            sort: { fields: [frontmatter___date], order: DESC }
            filter: { fields: {pageType: {eq: "portfolio"}} }
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        title
                        featuredImage {
                            childImageSharp {
                                fluid(maxWidth: 600, maxHeight:600, fit:CONTAIN, background:"rgba(0, 0, 0, 0.0)") {
                                    src
                                    srcSet
                                    aspectRatio
                                    sizes
                                    base64
                                }
                            }
                            publicURL
                        }
                    }
                    fields {
                        slug
                    }
                }
            }
        }
    }
    `)

    return (
        <SectionContainer title="Portfolio" fullWidth>
            <ul className="flex flex-wrap">
                {data.items.edges.map(({ node }) => (
                    <li key={node.id} className="w-1/2 block sm:w-1/3 lg:w-1/4 xl:w-1/5">
                        <Fade fraction={0}>
                            <PortfolioItem item={node} />
                        </Fade>
                    </li>
                ))}
            </ul>
        </SectionContainer>
    )
}

export default PortfolioSection
