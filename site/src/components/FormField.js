/* eslint-disable react/jsx-props-no-spreading */
import React from "react"
import * as PropTypes from "prop-types"
import Typography from "./Typography"

const FormField = ({
    type, value, label, name, ...inputProps
}) => {
    const Component = type === "textarea" ? "textarea" : "input"
    return (
        <div className="mb-4">
            { label && (
                <Typography component="label" htmlFor={name} color="inherit">{label}</Typography>
            )}
            <Component
                className="bg-black10 block w-full p-2 text-dark shadow"
                name={name}
                value={value}
                type={type}
                {...inputProps}
            />
        </div>
    )
}

FormField.propTypes = {
    type: PropTypes.oneOf(["textarea", "text", "email"]),
    value: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
}

FormField.defaultProps = {
    type: "input",
    value: "",
    label: undefined,
}

export default FormField
