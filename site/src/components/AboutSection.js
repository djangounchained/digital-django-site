import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"
import { Fade } from "react-awesome-reveal"
import Typography from "./Typography"
import SectionContainer from "./SectionContainer"

const AboutSection = () => {
    const { image } = useStaticQuery(graphql`
        query {
            image: file(
                sourceInstanceName: {eq: "images"}
                name: {eq: "profile"}
            ) {
                childImageSharp {
                    fixed(width: 224) {
                        ...GatsbyImageSharpFixed
                    }
                }
            }
        }
    `)

    return (
        <SectionContainer darkMode>
            <Typography variant="h2" color="primary" align="right">
                DigitalDjango
            </Typography>
            <Typography variant="h3" color="light" align="right" className="mb-16">
                Software engineering
            </Typography>
            <div className="flex flex-wrap">

                <div className="w-full sm:w-1/2 text-center">
                    <Fade left triggerOnce>
                        <Img
                            className="rounded-full mx-auto mb-8"
                            fixed={image.childImageSharp.fixed}
                        />
                    </Fade>
                </div>
                <div className="w-full sm:w-1/2">
                    <Fade right triggerOnce>
                        <Typography color="inherit" className="mb-8">
                            I’m a lead software engineer with more than ten years&rsquo; experience at all
                            stages of the development lifecycle.  Whether you’re a startup with an
                            exciting idea you’re ready to bring to life or a more established company
                            seeking to scale up your development efforts to meet the demands of a
                            growing business, I can help accomplish your goals expertly and efficiently.
                        </Typography>
                    </Fade>
                </div>
            </div>
        </SectionContainer>
    )
}

export default AboutSection
