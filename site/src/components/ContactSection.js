import React, { useReducer, useState } from "react"
import qs from "qs"
import axios from "axios"
import { Bounce, Fade } from "react-awesome-reveal"

import FormField from "./FormField"
import Button from "./Button"
import SectionContainer from "./SectionContainer"
import Typography from "./Typography"

const ContactSection = () => {
    const [state, dispatch] = useReducer(
        (s, { field, value }) => ({ ...s, [field]: value }),
        {
            name: "", email: "", message: "", captcha: undefined,
        },
    )
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [error, setError] = useState(undefined)
    const [isSuccess, setIsSuccess] = useState(false)

    const onChange = (e) => {
        dispatch({ field: e.target.name, value: e.target.value })
    }

    const onSubmit = (values) => {
        setError(undefined)
        setIsSubmitting(true)
        return axios.post(
            process.env.CONTACT_FORM_URL,
            qs.stringify(values),
        )
            .then(() => { setIsSuccess(true) })
            .catch((e) => { setError(e) })
            .finally(() => { setIsSubmitting(false) })
    }

    const { name, email, message } = state
    let errorMessage
    if (error) {
        if (error.response && error.response.data && error.response.data.error) {
            errorMessage = error.response.data.error
        } else {
            errorMessage = error.message
        }
    }
    return (
        <SectionContainer title="Contact" darkMode>
            {!!errorMessage && (
                <div>
                    <Typography color="light" className="p-2 mb-8 bg-accent rounded shadow">{errorMessage}</Typography>
                </div>
            )}
            {/* <Fade right when={!!errorMessage} duration={250} mountOnEnter unmountOnExit>
                
            </Fade> */}

            <form
                className="relative h-72"
                onSubmit={(e) => {
                    e.preventDefault()
                    return onSubmit(state)
                }}
            >
                {isSuccess && (
                    <div className="w-full absolute">
                        <Typography variant="h3" color="accent" align="center" gutterBottom>Thank you!</Typography>
                        <Typography color="light" align="center" gutterBottom>
                            Your message was sent successfully.<br />
                            I will contact you as soon as possible.
                        </Typography>
                    </div>
                )}

                {!isSuccess && (
                    <Bounce fraction={0.5} when={!isSuccess} triggerOnce>
                        <div className="w-full absolute">
                            <FormField
                                label="Name"
                                type="text"
                                name="name"
                                disabled={isSubmitting}
                                value={name}
                                onChange={onChange}
                            />
                            <FormField
                                label="E-mail"
                                type="email"
                                name="email"
                                disabled={isSubmitting}
                                value={email}
                                onChange={onChange}
                            />
                            <FormField
                                label="Message"
                                type="textarea"
                                name="message"
                                disabled={isSubmitting}
                                value={message}
                                onChange={onChange}
                            />
                            <Button
                                disabled={isSubmitting}
                                showLoader={isSubmitting}
                                className="float-right clearfix"
                            >
                                Submit
                            </Button>
                        </div>
                    </Bounce>
                )}
            </form>
        </SectionContainer>
    )
}

export default ContactSection
