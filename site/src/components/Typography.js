/* eslint-disable react/jsx-props-no-spreading */
import React from "react"
import * as PropTypes from "prop-types"
import classNames from "classnames"

const fontSizes = {
    h1: "text-xxl",
    h2: "text-xl",
    h3: "text-lg",
    h4: "text-md",
    h5: "text-sm",
    body1: "text-md",
    body2: "text-sm",
}

const lineHeights = {
    h1: "leading-xl",
    h2: "leading-lg",
    h3: "leading-md",
    h4: "leading-none",
    h5: "leading-sm",
    body1: "leading-none",
    body2: "leading-sm",
}

const weights = {
    h1: "font-light",
    h2: "font-light",
    h3: "font-light",
    h4: "font-semibold",
    h5: "font-semibold",
    body1: "font-normal",
    body2: "font-normal",
}

const Typography = ({
    children, variant, component, inline, align, color, gutterBottom, className, ...elementProps
}) => {
    const Component = component
    return (
        <Component
            className={classNames([
                "font-sans",
                fontSizes[variant],
                lineHeights[variant],
                weights[variant],
                `text-${align}`,
                `text-${color}`,
                inline ? "inline" : "block",
                gutterBottom ? "mb-4" : undefined,
                className,
            ])}
            {...elementProps}
        >
            {children}
        </Component>
    )
}

Typography.propTypes = {
    children: PropTypes.node,
    component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    variant: PropTypes.oneOf([
        "h1",
        "h2",
        "h3",
        "h4",
        "h5",
        "body1",
        "body2",
    ]),
    inline: PropTypes.bool,
    align: PropTypes.oneOf([
        "left",
        "right",
        "center",
        "justify",
    ]),
    color: PropTypes.oneOf([
        "default",
        "inherit",
        "transparent",
        "primary",
        "secondary",
        "accent",
        "light",
    ]),
    gutterBottom: PropTypes.bool,
    className: PropTypes.string,
}

Typography.defaultProps = {
    children: undefined,
    variant: "body1",
    component: "p",
    inline: false,
    align: "left",
    color: "default",
    gutterBottom: false,
    className: undefined,
}

export default Typography
