import React from "react"
import Typography from "./Typography"

const Footer = () => (
    <div className="bg-secondary">
        <div className="container px-4 pt-16 pb-4 text-light">
            <Typography variant="body2" color="inherit">&copy; Digital django 2019</Typography>
        </div>
    </div>
)

export default Footer
