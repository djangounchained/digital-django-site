import React, { useRef, useEffect } from "react"
import { graphql, Link, useStaticQuery } from "gatsby"
import Img from "gatsby-image"
import Typography from "./Typography"
import Button from "./Button"

const MainHeaderSection = () => {
    const { desktopImage, mobileImage } = useStaticQuery(graphql`
        query {
            desktopImage: file(
                sourceInstanceName: {eq: "images"}
                name: {eq: "background"}
            ) {
                childImageSharp {
                    fluid(maxWidth: 1980, maxHeight:1080) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
            mobileImage: file(
                sourceInstanceName: {eq: "images"}
                name: {eq: "background-mobile"}
            ) {
                childImageSharp {
                    fluid(maxWidth: 768, maxHeight:1024) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
        }
    `)

    const parallaxElement = useRef()

    useEffect(() => {
        const updateParallaxElement = () => {
            const node = parallaxElement.current
            if (!node) { return }

            if (window.pageYOffset === 0) {
                node.style.top = "0px"
                return
            }

            if (window.pageYOffset > window.screen.height) {
                return
            }

            const currentOffset = parseInt(node.offsetTop, 10)
            const newPos = Math.floor(currentOffset + ((Math.floor(window.pageYOffset / 1.5) - currentOffset)))
            node.style.top = `${newPos}px`
        }
        window.addEventListener("scroll", updateParallaxElement)
        return () => window.removeEventListener("scroll", updateParallaxElement)
    }, [])

    return (
        <div className="h-screen2/3 lg:h-screen4/5 xl:screen5/6 relative overflow-hidden">
            <div ref={parallaxElement} className="absolute z-10 h-full w-full">
                <Img
                    style={{ height: "100%" }}
                    imgStyle={{ objectFit: "cover" }}
                    fluid={[
                        mobileImage.childImageSharp.fluid,
                        {
                            ...desktopImage.childImageSharp.fluid,
                            media: "(min-width: 768px)",
                        },
                    ]}
                />
            </div>
            <div className="absolute w-full h-full z-30">
                <div className="flex flex-col justify-center items-center w-full h-full">
                    <Typography variant="h1" color="light" align="center">I build apps.</Typography>
                    {/* <Button
                        component={Link}
                        to="/trainees"
                    >
                        Traineeship
                    </Button> */}
                </div>
            </div>
        </div>
    )
}

export default MainHeaderSection
