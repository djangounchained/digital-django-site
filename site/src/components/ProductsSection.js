import React from "react"
import { Fade } from "react-awesome-reveal"
import { MdImportantDevices } from "react-icons/md"
import { IoMdSchool, IoMdCodeWorking, IoMdCog } from "react-icons/io"
import SectionContainer from "./SectionContainer"
import ProductItem from "./ProductItem"

const products = [
    {
        title: "Development",
        description: "As a full stack developer I have experience with various frontend, "
            + "backend and cloud technologies. So whether you need assistance with an Android, "
            + "iOS or web application, or something else entirely... I can guarantee "
            + "a stable and scalable product.",
        icon: IoMdCodeWorking,
    },
    {
        title: "Concept to Market",
        description: "I'll lend my creativity and expertise to helping you get your "
            + "next great idea off the ground – let's collaborate during the initial "
            + "stages of your project to turn your idea into working a prototype. "
            + "We'll use scalable methods from day one so your product can evolve seamlessly.",
        icon: MdImportantDevices,
    },
    {
        title: "Architecture",
        description: "A solid architecture is integral to a product's long-term success. "
            + "If your development team is facing challenges of scale or productivity, "
            + "I can assess your current needs and work with your team to ensure "
            + "you have the architecture in place to handle whatever comes your way.",
        icon: IoMdCog,
    },
    {
        title: "Team Lead / Coaching",
        description: "I create value by sharing knowledge and setting your team up for success. My proven, "
            + "time-tested development and organizational strategies can help you and your team increase productivity "
            + "and ultimately improve the quality of your product.",
        icon: IoMdSchool,
    },
]

const ProductsSection = () => (
    <SectionContainer title="Services" fullWidth>
        <Fade bottom cascade fraction={0.3} triggerOnce>
            <ul className="flex flex-wrap -mx-4">
                {products.map((product) => (
                    <li
                        className="w-full px-4 sm:w-1/2 lg:w-1/4"
                        key={product.title}
                    >
                        <ProductItem
                            title={product.title}
                            description={product.description}
                            icon={product.icon}
                        />
                    </li>
                ))}
            </ul>
        </Fade>
    </SectionContainer>
)

export default ProductsSection
