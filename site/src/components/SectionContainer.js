import React from "react"
import * as PropTypes from "prop-types"
import classNames from "classnames"
import Typography from "./Typography"

const SectionContainer = ({
    title, darkMode, children, fullWidth,
}) => (
    <div
        className={classNames({
            "bg-secondary": darkMode,
            "text-light": darkMode,
            "bg-grey2": !darkMode,
            "text-dark": !darkMode,
        })}
    >
        <div className={classNames(["container", "mx-auto", "px-4", "pt-4", "pb-16", { "max-w-3xl": !fullWidth }])}>
            {title && (
                <Typography variant="h2" color="primary" className="py-12" align="center">{title}</Typography>
            )}
            {children}
        </div>
    </div>
)

SectionContainer.propTypes = {
    title: PropTypes.string,
    darkMode: PropTypes.bool,
    children: PropTypes.node,
    fullWidth: PropTypes.bool,
}

SectionContainer.defaultProps = {
    title: undefined,
    children: "",
    darkMode: false,
    fullWidth: false,
}

export default SectionContainer
