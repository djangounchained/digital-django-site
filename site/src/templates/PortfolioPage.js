import React from "react"
import * as PropTypes from "prop-types"
import { graphql } from "gatsby"
import Layout from "../components/Layout"
import Typography from "../components/Typography"
import SEO from "../components/seo"

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        website
        status
        tech
        noindex
        featuredImage {
            childImageSharp {
                fluid(maxWidth: 600, maxHeight:256) {
                    src
                    srcSet
                    aspectRatio
                    sizes
                    base64
                }
            }
            publicURL
        }
      }
    }
  }
`

const PortfolioPage = ({ data }) => {
    const portfolioItem = data.markdownRemark
    const meta = []
    if (portfolioItem.frontmatter.noindex === true) {
        meta.push({
            name: "robots",
            content: "noindex",
        })
    }
    return (
        <Layout title="Portfolio">
            <SEO
                title={`Digital django | ${portfolioItem.frontmatter.title}`}
                description="Django Witmer. Experienced Software Engineer &amp; Tech Lead."
                meta={meta}
            />
            <Typography variant="h1" className="mb-16">{portfolioItem.frontmatter.title}</Typography>

            <ul className="flex flex-row flex-wrap mb-2">
                {portfolioItem.frontmatter.tech.map((item) => (
                    <li key={item} className="rounded px-2 py-1 mr-4 mb-2 bg-black10">
                        <Typography variant="body2">{item}</Typography>
                    </li>
                ))}
            </ul>

            <Typography variant="body2" className="mb-2">
                { portfolioItem.frontmatter.website && (
                    <a
                        className="mr-4 text-primary hover:bg-black10 simpleTransition"
                        href={portfolioItem.frontmatter.website}
                        target="__blank"
                    >Visit site
                    </a>
                )}
                { portfolioItem.frontmatter.status && (
                    <Typography component="span" color="secondary" variant="h5" inline>
                        Status: {portfolioItem.frontmatter.status}
                    </Typography>
                )}
            </Typography>

            <Typography component="div" dangerouslySetInnerHTML={{ __html: portfolioItem.html }} />
        </Layout>
    )
}

PortfolioPage.propTypes = {
    data: PropTypes.shape({
        markdownRemark: PropTypes.shape({
            frontmatter: PropTypes.shape({
                title: PropTypes.string.isRequired,
                tech: PropTypes.arrayOf(PropTypes.string),
                website: PropTypes.string,
                status: PropTypes.string,
                noindex: PropTypes.bool,
            }).isRequired,
            html: PropTypes.string,
        }).isRequired,
    }).isRequired,
}

PortfolioPage.defaultProps = {
}

export default PortfolioPage
