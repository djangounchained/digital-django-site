import React from "react"

import SEO from "../components/seo"
import Footer from "../components/Footer"

const TraineesPage = () => (
    <div>
        <SEO
            title="Traineeship"
            description="Traineeship at DigitalDjango"
        />
        <Footer />
    </div>
)

export default TraineesPage
