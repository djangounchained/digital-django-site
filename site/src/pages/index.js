import React from "react"

import SEO from "../components/seo"
import MainHeaderSection from "../components/MainHeaderSection"
import AboutSection from "../components/AboutSection"
import ContactSection from "../components/ContactSection"
import PortfolioSection from "../components/PortfolioSection"
import ProductsSection from "../components/ProductsSection"
import Footer from "../components/Footer"

const IndexPage = () => (
    <div>
        <SEO
            title="Digital django"
            description="Django Witmer. Experienced Software Engineer &amp; Tech Lead."
        />

        <MainHeaderSection />

        <AboutSection />

        <ProductsSection />

        <ContactSection />

        <PortfolioSection />

        <Footer />
    </div>
)

export default IndexPage
