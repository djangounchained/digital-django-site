---
title: "Bridgefund"
website: "https://www.bridgefund.nl/"
status: "Production"
date: 2022-06-02
featuredImage: bf_logo.png
noindex: True
tech: ["Kotlin", "Android", "Swift", "iOS", "Choaching"]
---
BridgeFund is een platform dat een brug slaat tussen de financiële behoeftes van ondernemers. Zonder tussenkomst van een bank.

Met DigitalDjango hebben wij de volledige ontwikkeling van de Bridgefund app voor zowel Android als iOS op ons genomen.

![Bridgefund screenshot](bf_screenshot1.png)
