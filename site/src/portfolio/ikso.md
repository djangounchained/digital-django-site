---
title: "IKSO"
website: "https://ikso.nl/"
date: 2017-06-01
featuredImage: ikso_logo.png
tech: ["PHP", "Laravel", "Javascript", "JQuery", "Postgres", "Html", "CSS", "Android", "Java"]
---

A growth and development platform that helps people in a team give each other open and honest feedback. I built the platform from the ground up, stayed on as their only developer through four iterations of the platform, and later trained a successor to take over my work. The platform is still going strong today.

![IKSO screenshot 1](ikso_screenshot1.jpg)
![IKSO screenshot 2](ikso_screenshot2.png)
![IKSO screenshot 3](ikso_screenshot3.png)