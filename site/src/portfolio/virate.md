---
title: "Virate"
status: "Discontinued"
date: 2015-10-01
featuredImage: virate_logo.png
tech: ["Swift", "iOS"]
---

An app for making and posting short video reviews of everyday objects. After the previous developer left, I took this project over the finish line.

![Virate screenshot 1](virate_screenshot1.jpg)
