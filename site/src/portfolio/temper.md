---
title: "Temper"
website: "https://temper.works/"
date: 2019-02-05
status: "Production"
featuredImage: temper_logo.png
tech: ["Javascript", "Vue", "Tailwind", "PHP", "Laravel", "Mysql", "Codeship", "Optimizely"]
---
A modern marketplace for freelancers focused on the Horeca. Temper helps thousands of people find freelance work in the Netherlands each week. As Engineering lead my contributions included creating architecture, planning, coaching, creating development workflows, ensuring on-time delivery, promoting a test-driven approach to development and streamlining communication between teams (API, web front-end, and app developers). As product team lead, my primary focus has been to increase user retention by making the Temper platform more user-friendly.

![Temper screenshot 1](temper_screenshot1.png)
![Temper screenshot 2](temper_screenshot2.png)
