---
title: "Colect"
website: "https://www.colect.io/"
date: 2021-05-01
status: "Production"
featuredImage: colect_logo.png
tech: ["Objective-c", "iOS", "TvOS", "Coredata"]
---

This state of the art app is your sales reps best friend. It enables amazing product presentations, increase sales and offers a smooth order intake process. Our goal here as interum tech lead was to ensure development on their iOS and TvOS apps continued during a leadership transition.

![Collect screenshot 1](colect_screenshot1.jpg)
![Collect screenshot 2](colect_screenshot2.jpg)
![Collect screenshot 3](colect_screenshot3.jpg)
