---
title: "Xxiyou"
website: ""
status: "Discontinued"
date: 2014-01-01
featuredImage: xxiyou_logo.png
tech: ["Objective-C", "iOS", "Java", "Android"]
---

An app that would allow shoppers to get discounts on selected products. The client was ultimately unable to make good deals with shops and failed to attract a user base. I built the iOS and Android apps from the ground up and worked on various iterations of the platform. This was back when Android was at version 2.2 and iOS 5 just came out.

![Xxiyou screenshot 1](xxiyou_screenshot1.png)
![Xxiyou screenshot 2](xxiyou_screenshot2.jpg)
