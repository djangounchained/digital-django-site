---
title: "Memorix Maior"
website: "https://picturae.com/en/dsh#collection-management"
date: 2011-01-01
featuredImage: memorix_logo.png
tech: ["PHP", "Zend framework", "Javascript", "jQuery", "Html", "CSS", "Postgres"]
---

A web application for managing and creating digital collections for cultural heritage. As Junior engineer I was part of a team working on the platform. I also did a lot of database work.

![Memorix screenshot 1](memorix_screenshot1.jpg)
