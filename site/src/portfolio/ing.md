---
title: "Lead Android developer ING"
website: "https://ing.nl/"
status: "Production"
date: 2022-06-01
featuredImage: ing_logo.png
noindex: True
tech: ["Kotlin", "Android", "Choaching", "Security", "CI", "Unit testing"]
---
At ING I worked on two modules of the mobile banking app. Onboarding to allow more user to simply onboard all in the app and in a simple way. Enrollment to let users login using their ID. This makes the mobile banking app easier to use and prevents a lot of fraud.
In my role as lead Android developer I worked on the technical design and architecture of these modules, I streamlined communication between my team and other teams. As well as hands on coding on the new modules.

![ING screenshot](ing_screenshot1.jpg)
