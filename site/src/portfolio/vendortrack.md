---
title: "Interum CTO Vendortrack"
website: "https://vendortrack.io/"
status: "Discontinued"
date: 2017-12-18
featuredImage: vendortrack_logo.png
noindex: True
tech: ["Python", "Flask", "Javascript", "React", "Postgres", "Material UI", "Google cloud functions", "Gitlab CI", "HTML", "CSS"]
---
Vendortrack, a promising startup, allows teams to bring on new suppliers effortlessly and keep them compliant automatically. As interum CTO I was responsible for the initial technical side of the business and product direction as well as hands on coding.
I setup scalable infrastructure including continues integration and automated testing. Some of the challenges involved setting up international infrastructure that is both fast anywhere on the planet and can be compliant with local privacy and securiy laws. I also setup the workflows for the development team and coordinated with the design department.

![Vendortrack screenshot](vendortrack_screenshot1.png)
