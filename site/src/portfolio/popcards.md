---
title: "Popcards"
status: "Discontinued"
date: 2015-10-03
featuredImage: popcards_logo.png
tech: ["Swift", "iOS", "Kotlin", "Android", "Native Camera APIs", "Layar augmented reality SDK"]
---

An app for making AR postcards. Users could take a video, build a card with a photo, and send the card, The recipient was able to point their camera at the card, and the video would play in place of the photo using AR. I built both iOS and Android apps from the ground up. These apps required implementation of the camera APIs, integration with the client’s backend for sending postcards and payment, and Layar’s AR platform.

![Popcards screenshot 1](popcards_screenshot1.png)
