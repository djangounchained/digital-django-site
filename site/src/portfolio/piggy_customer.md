---
title: "Piggy"
website: "https://www.piggy.eu/"
date: 2020-11-05
status: "Production"
featuredImage: piggy_customer_logo.png
tech: ["Kotlin", "Android", "Choaching", "Team lead", "Push notifications", "CI", "Unit testing", "Image recognition", "Full text search", "Adyen integration"]
---

Piggy makes loyalty programs fun and easy! With only one loyalty card or the app you can save points at all your favorite stores.
As lead Android developer I was responsible for two Android applications. The store app runs on android tablets in stores all over the Netherlands and Germany.
The store app can use image recognition to scan the users loyalty card. It can also be connected to existing cash register systems to automatically trigger points saving.
The customer app, with over 100K installs, is used by users to view their savings and to promote stores and special offers. It can also link the users bank account so points can be automatically saved without even scanning their card.
Als lead android dev I was responsible for architecture, coding, workflow and training of new team members.

![Piggy screenshot 1](piggy_customer1.png)
![Piggy screenshot 2](piggy_customer2.png)
![Piggy screenshot 3](piggy_customer3.png)
![Piggy screenshot 4](piggy_customer4.png)