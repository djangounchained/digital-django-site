---
title: "Appical"
website: "https://appical.net/"
date: 2018-04-01
status: "Production"
featuredImage: appical_logo.jpg
tech: ["Swift", "iOS", "Kotlin", "Android", "Push notifications", "RX", "Realm", "Javascript", "React", "Material UI", "Html", "CSS", "Circle CI"]
---

An app for onboarding new employees in an organization. In my role as Engineering Lead I led my team to build a brand new mobile app from scratch in under four months using native Kotlin and Swift. After completing this project, I was tasked with updating our web app. To accomplish this, I learned React and recruited a new team. I completed this project in under four months as well. 

My main responsibilities for this project included: defining and implementing architecture on Android, iOS and React; defining and implementing workflows; researching and implementing new tech; translating user and business requirements to technical design; coaching and leading my team (which ultimately consisted of ten people).

![Appical screenshot 1](appical_screenshot1.jpg)
![Appical screenshot 2](appical_screenshot2.png)
![Appical screenshot 3](appical_screenshot3.jpg)
