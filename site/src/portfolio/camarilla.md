---
title: "Camarilla"
status: "Discontinued"
date: 2017-05-05
featuredImage: camarilla_logo.jpg
tech: ["C#", "Xamarin", "Android"]
---

An exciting but ultimately unsuccessful social media startup. As Lead Android Engineer I was in charge of the Android team (myself and two others). When I began, the existing code was in a very bad state. I made sure to refactor the code while still releasing new features at the same time. I was able to increase productivity by 50% by cleaning up the code and enabling developers to work more efficiently. I also advised the iOS team.

![Camarilla screenshot 1](camarilla_screenshot1.jpg)
