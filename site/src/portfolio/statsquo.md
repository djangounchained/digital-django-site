---
title: "statsQuo"
status: "Discontinued"
date: 2015-10-02
featuredImage: statsquo_logo.jpg
tech: ["Swift", "iOS", "PHP", "Laravel", "Html", "CSS", "Postgres", "PostGIS"]
---

Another exciting but ultimately failed social media startup. I built the platform from the ground up. I implemented various iterations of the platform including one with advanced location based functionality.

![Statsquo screenshot 1](statsquo_screenshot1.png)
![Statsquo screenshot 2](statsquo_screenshot2.png)
![Statsquo screenshot 3](statsquo_screenshot3.jpeg)