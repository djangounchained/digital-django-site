---
title: "Gigstarter"
website: "https://www.gigstarter.nl/"
date: 2021-08-01
status: "Production"
featuredImage: gigstarter_logo.png
tech: ["Swift", "iOS", Kotlin, Android]
---

Gigstarters mission is to make bookings of live music more accesible. We made brand new iOS and Android apps for them. They work just like tinder but you see artists that are looking for gigs.

<!-- ![Appical screenshot 1](colect_screenshot1.jpg)
![Appical screenshot 2](colect_screenshot2.jpg)
![Appical screenshot 3](colect_screenshot3.jpg) -->
