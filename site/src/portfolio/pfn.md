---
title: "Push for News"
status: "Discontinued"
date: 2016-01-01
featuredImage: pfn_logo.png
tech: ["Swift", "iOS", "Google news API"]
---

As the main developer I built the iOS app from the ground up. The app had to feel very smooth and look amazing. I worked with a design team and implemented various smooth animations.

![Push for news screenshot 1](pfn_screenshot1.png)
