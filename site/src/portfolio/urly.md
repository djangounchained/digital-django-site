---
title: "Urly"
status: "Discontinued"
date: 2017-08-01
featuredImage: urly_logo.png
tech: ["Swift", "iOS", "Push notifications", "Python", "Flask", "Postgres", "Javascript", "React", "Html", "CSS", "Google cloud functions"]
---

An iOS app for staying up-to-date on dance events in the Netherlands that notifies users of early bird ticket sales. I single handedly built the complete platform from the ground up.

![URLY screenshot 1](urly_screenshot1.jpg)
