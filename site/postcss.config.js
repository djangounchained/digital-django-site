const tailwind = require(`tailwindcss`)
// const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = () => ({
    plugins: [
        tailwind,
        // purgecss({
        //     content: [
        //         `./src/**/*.js`,
        //         `./src/**/*.html`,
        //         `./public/index.html`,
        //     ],
        //     whitelistPatterns: [/^text-/, /^leading-/, /^font-/],
        // })
    ],
})
