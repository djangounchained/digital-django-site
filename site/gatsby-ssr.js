const React = require("react")

exports.onRenderBody = (
    { setPostBodyComponents }
) => {
    if (process.env.NODE_ENV === "production") {
        setPostBodyComponents([
            <script src="/__/firebase/7.6.1/firebase-app.js"></script>,
            <script src="/__/firebase/7.6.1/firebase-analytics.js"></script>,
            <script src="/__/firebase/init.js"></script>,
        ])
    }
}
