module.exports = {
  theme: {
    fontFamily: {
      sans: [`Montserrat`, `sans-serif`],
    },
    colors: {
      black: "#000000",
      black10: "#F3F3EC",
      black2: "#FFFFFA",
      primary: "#FA824C",
      primary70: "rgba(250, 131, 76, 0.7)",
      secondary: "#444054",
      accent: "#FF6663",
    },
    backgroundColor: theme => theme("colors"),
    textColor: theme => {
      const { black, black2, primary, secondary, accent } = theme("colors")
      return {
        default: black,
        transparent: "transparent",
        inherit: "inherit",
        primary,
        secondary,
        accent,
        dark: black,
        light: black2,
      }
    },
    fontSize: {
      sm: "0.8125rem",
      md: "1rem",
      lg: "1.5rem",
      xl: "2rem",
      xxl: "3rem",
    },
    lineHeight: {
      none: "1.5rem",
      sm: "1rem",
      md: "2rem",
      lg: "3rem",
      xl: "4rem",
    },
    extend: {
      height: {
        "72": "18rem",
        "screen2/3": "66vh", 
        "screen3/4": "75vh",
        "screen4/5": "80vh",
        "screen5/6": "83vh",
      },
    },
  },
  variants: {},
  plugins: []
}
