module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },
    parser: "babel-eslint",
    extends: [
        // "react-app",
        "airbnb",
        // "plugin:jsx-a11y/recommended",
    ],

    rules: {
        "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",

        indent: ["error", 4],

        "max-len": ["error", 120],
        quotes: ["error", "double"],
        semi: ["error", "never"],
        "import/no-named-as-default": "off",
        "import/extensions": ["error", "never"],
        "import/prefer-default-export": "off",

        "react/jsx-indent": ["error", 4],
        "react/jsx-indent-props": ["error", 4],
        "react/jsx-filename-extension": ["warn", { "extensions": [".js"] }],

        "react/jsx-one-expression-per-line": "off",

        "react/forbid-prop-types": "off",

        "no-param-reassign": ["error", { "props": true, "ignorePropertyModificationsFor": ["state"] }],

        "import/no-extraneous-dependencies": ["error", {
            devDependencies: [ // Rule is ignored for these files
                "/src/setupTests.js",
            ],
            optionalDependencies: false,
            peerDependencies: false,
        }],
    },
    // parserOptions: {
    //     parser: "babel-eslint",
    // },
    overrides: [
        // Override to have proper linting in test files
        Object.assign(
            require("eslint-plugin-jest").configs.recommended, // eslint-disable-line global-require
            {
                plugins: ["jest"],
                env: {
                    jest: true,
                },
                files: [
                    "setupTests.js",
                    "**/__tests__/*.js",
                    "**/__mocks__/*.js",
                ],
                rules: {
                },
            },
        ),
    ],
}
