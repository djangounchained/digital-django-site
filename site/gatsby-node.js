const { createFilePath } = require("gatsby-source-filesystem")

exports.onCreateNode = ({ node, getNode, actions }) => {
    const { createNodeField } = actions
    if (node.internal.type === "MarkdownRemark") {
        const { sourceInstanceName } = getNode(node.parent)
        const slug = createFilePath({
            node, getNode, basePath: sourceInstanceName, trailingSlash: false,
        })

        createNodeField({ node, name: "slug", value: `/${sourceInstanceName}${slug}` })
        createNodeField({ node, name: "pageType", value: sourceInstanceName })
    }
}

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions

    const result = await graphql(`
        query {
            allMarkdownRemark {
                edges {
                    node {
                        fields {
                            slug
                        }
                    }
                }
            }
        }
    `)

    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
        createPage({
            path: node.fields.slug,
            component: require.resolve("./src/templates/PortfolioPage.js"),
            context: {
                slug: node.fields.slug,
            },
        })
    })
}
