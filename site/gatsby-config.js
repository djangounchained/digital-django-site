module.exports = {
    siteMetadata: {
        title: "Freelance software engineer",
        description: "Software engineering and development.",
        author: "@digitaldjango",
    },
    plugins: [
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "portfolio",
                path: `${__dirname}/src/portfolio`,
            },
        },
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: `${__dirname}/src/images`,
            },
        },
        "gatsby-transformer-sharp",
        "gatsby-plugin-sharp",
        {
            resolve: "gatsby-transformer-remark",
            options: {
                plugins: [
                    {
                        resolve: "gatsby-remark-images",
                        options: {
                            maxWidth: 1280,
                        },
                    },
                ],
            },
        },
        "gatsby-plugin-postcss",
        "gatsby-plugin-react-helmet",
        // "gatsby-plugin-transition-link",
        {
            resolve: "gatsby-plugin-manifest",
            options: {
                name: "gatsby-starter-default",
                short_name: "starter",
                start_url: "/",
                background_color: "#444054",
                theme_color: "#444054",
                display: "minimal-ui",
                icon: "src/images/dd-icon.png", // This path is relative to the root of the site.
            },
        },
    // `gatsby-plugin-offline`,
    ],
}
