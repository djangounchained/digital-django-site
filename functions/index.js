const functions = require("firebase-functions")
const admin = require("firebase-admin")
const nodemailer = require("nodemailer")
// const cors = require("cors")({ origin: true })

admin.initializeApp()

const mailConfig = functions.config().email

const transporter = nodemailer.createTransport({
    host: mailConfig.host,
    secure: true,
    port: 465,
    auth: {
        user: mailConfig.username,
        pass: mailConfig.password,
    },
})

transporter.verify(function(error, success) {
    if (error) {
        // console.log(config)
        console.log(mailConfig)
        console.log(error);
    } else {
        console.log("Server is ready to take our messages");
    }
});

function EmailException(e) {
    const error = new Error(e);
    error.code = 500
    return error
}
EmailException.prototype = Object.create(Error.prototype);

function ValidationException(message) {
    const error = new Error(message)
    error.code = 400
    return error
}
ValidationException.prototype = Object.create(Error.prototype);

function sendMail(mailOptions) {
    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                reject(err)
                return
            }
            resolve(info)
        })
    })
}

// async function validateReCaptchaToken(token) {
//     const response = await axios.post(
//         "https://recaptcha.google.com/recaptcha/api/siteverify",
//         {
//             secret: captchaConfig.token,
//             response: token,
//         },
//     )

//     return response.success
// }

async function validateRequest(body) {
    if (!body.name || body.name.length < 2) {
        throw new ValidationException("Please tell me your name.")
    }
    if (!body.email || body.email.length < 3) {
        throw new ValidationException("Please provide a valid E-mail address.")
    }
    if (!body.message || body.message.length < 2) {
        throw new ValidationException("Message is required.")
    }
    // if (!body.captcha || !await validateReCaptchaToken(body.captcha)) {
    //     throw new ValidationException("Invalid Captcha. Are you a robot?")
    // }

    return {
        email: body.email,
        name: body.name,
        message: body.message
    }
}

function buildEmailBody(name, email, text) {
    return "----------------\n\
name : " + name + "\n\
email : " + email + "\n\
----------------\n\
" + text
}

exports.sendMail = functions.https.onRequest(async (request, response) => {
    // cors(request, response, async () => {
        try {
            const result = await validateRequest(request.body)

            const mailOptions = {
                from: "machine@digitaldjango.com",
                to: "django@digitaldjango.com",
                replyTo: result.name + "<" + result.email + ">",
                subject: "DigitalDjango contact form",
                text: buildEmailBody(result.name, result.email, result.message),
            }
    
            
            return await sendMail(mailOptions)
                .then(() => { return response.send("Done") })
                .catch((e) => {
                    console.log(e)
                    return response.status(500).send({ error: e.toString() })
                })
        } catch (e) {
            console.log("Caught error!", e)
            return response.status(
                (e.code ? e.code : 500)
            ).send({ error: e.toString() })
        }
    // })
})
