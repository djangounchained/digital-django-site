# digitaldjango.com

## Site
See /site/readme.md

## Email sender function
### Development

* Set Environment variables
* `firebase functions:config:set email.host="SMTP HOST"`
* `firebase functions:config:set email.password="YOUR SMTP PASSWORD"`
* `firebase functions:config:set email.username="YOUR SMTP USERNAME"`
* `firebase functions:config:set email.receiver="THE EMAIL YOU WANT TO SEND TO"`
* `firebase functions:config:set email.sender="SENDER EMAIL"`

* Local development:
* `cd functions`
* `yarn serve`